<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$webTitle = 'About';
$webTitleNo = 5;
$angularJS = '';
// relative corrective placement of root path from index
$webRoot = '../';

require_once $webRoot . 'headerFooter/header.inc.php';

// Page content
require_once 'about.inc.php';
// /Page content

require_once $webRoot . 'headerFooter/footer.inc.php';
?>