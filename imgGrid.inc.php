<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/** 
 * the actual webpage 
 * as $angularJS var is grouped in some pages
 */
$webPage = strtolower($webTitle);
?>

<div class="container" data-ng-controller="<?= $angularJS ?>Controller"><div class="row">

		<!-- Filter box -->
		<div id="filter" class="col-md-3 col-sm-12">
			<button class="btn btn-primary btn-block hidden-md hidden-lg" 
							type="button" 
							data-toggle="collapse" 
							data-target="#filterBox" 
							aria-expanded="false" 
							aria-controls="filterBox">
				Filter results
			</button>
			<!-- see #filter .collapse -->
			<div class="collapse row" id="filterBox">
				<div class="col-md-12"><div class="well">
					<div class="form-group">
						<input class="form-control"
									 data-ng-model="search"
									 type="text"
									 placeholder="Search for...">
					</div>
<?php
if($webPage == 'works') {
?>
					<div class="form-group">
<?php 
	$searchType = [
		'Sculpture', 'Drawing', 'Print'
	];
	for ($i = 0; $i < count($searchType); $i++) {
	?>
						<div class="checkbox"><label>
								<input type="checkbox"
											 data-ng-model="searchType[<?= $i ?>]"
											 data-ng-true-value="<?= $searchType[$i]?>"
											 data-ng-false-value="false">
								<?= $searchType[$i] ?>
						</label></div>
	<?php } ?>
					</div>
<?php } ?>
				</div></div>
			</div>
		</div>

		<!-- Images of data -->
		<div id="imgGrid" class="col-md-9 col-sm-12 img-ratio-col">

<?php /*
 */
?>
			<!-- Here is my big problem get it right this time you stupid so called 'programmer -->
			<div class="col-md-4 col-sm-6 text-ellipsis-hover"
					 data-ng-repeat="
								data1 in data | 
								orderBy:'-year' | 
								filter: search |
								filter: filterType()
					 ">
				<div class="img-ratio-responsive">
					<div class="wrapper-img">
						<img alt="image"
								 data-ng-mousedown="fullScreenOn(data1.id)"
								 data-ng-src="../images/<?= strtolower($webTitle) ?>/icon/{{toImgSrc(data1.id)}}.jpg">
					</div>
				</div>
				<div class="text-ellipsis">
					<p class="text-center text-ellipsis">
						{{data1.title}}<br>
						<small>
<?php if($webPage == "works") { ?>
						{{sizeCm(data1.id)}}
<?php } else { ?>
						{{ifNotZero(data1.year)}}
<?php } ?>
						</small>
					</p>
					<p>`
						<br>`
					</p>
				</div>
			</div>
<?php /*
 */
?>

		</div>

	</div>

	<div id="fullScreen" class="modal modalDark"
			 aria-hidden="true"
			 aria-labelledby="titleModalLabel"
			 role="dialog"
			 tabindex="-1">
		<div class="modal-dialog modal-lg modal-responsive">
			<div class="modal-content">
				<!--Header-->
				<div class="modal-header">

					<!--Close button-->
					<button class="close" data-ng-mousedown="fullScreenOff()"
									type="button"
									data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>

					<!--Title-->
					<h4 id="titleModalLable" class="modal-title text-left">
						<small class="">Martyn Last</small>
						<span id="fullDataTitle"></span>
					</h4>

				</div>

				<!--Body-->
				<div class="modal-body clearfix">
					<div class="img-ratio-responsive">
						<div class="wrapper-img">
							<img id="imgFullScreen" alt="full image here">
						</div>
					</div>
					
					<div class="container well">
						<div class="row">
<?php if($webPage == 'works') { ?>
							<div class="col-xs-6">
								<h5 class="text-left">
									<span id="fullDataType"></span><br/>
									<span id="fullDataMaterial"></span><br/>
									<span id="fullDataSize"></span>
								</h5>
							</div>
<?php } ?>
							<div class="col-xs-6 <?php 
	if($webPage == 'works') {
			echo 'text-right'; } ?>">
								<h4>
									<span id="fullDataYear"></span><br/><br/>
<?php if($webPage == 'installations' || $webPage == 'performances') { ?>
									<span id="fullDataPlace"></span><br/>
<?php } ?>
									<span id="fullDataOwner"></span>
								</h4>
								<h5>
								</h5>
							</div>
<?php if(!($webPage == 'works')) { ?>
							<div class="col-xs-12">
								<h4>
									<span id="fullDataInfo"></span>
								</h4>
							</div>
<?php } ?>
						</div>
					</div>
				</div>
				

			</div>
		</div>	
	</div> <!-- /#fullScreen -->

</div> <!-- /container ng-controller -->