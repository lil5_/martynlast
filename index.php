<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$webTitle = 'Home';
$webTitleNo = 0;
$angularJS = '';
// relative corrective placement of root path from index
$webRoot = '';

require_once 'headerFooter/header.inc.php';

// Page content
require_once 'home.inc.php';
// /Page content

require_once 'headerFooter/footer.inc.php';
?>