<?php
class Secure {
  
  public function secure($content=false, $option="default") {

    $option = false; // Old version depricated part
    $content = (
      !empty($content)
        ? trim($content)
        : false
    );

    if(is_numeric($content))
      return preg_replace("@([^0-9])@Ui", "", $content);

    else if(is_bool($content))
      return ($content?true:false);

    else if(is_float($content))
      return preg_replace("@([^0-9\,\.\+\-])@Ui", "", $content);

    else if(is_string($content)) {
      if(filter_var ($content, FILTER_VALIDATE_URL))
        return $content;
      else if(filter_var ($content, FILTER_VALIDATE_EMAIL))
        return $content;
      else if(filter_var ($content, FILTER_VALIDATE_IP))
        return $content;
      else if(filter_var ($content, FILTER_VALIDATE_FLOAT))
        return $content;
      else
        return preg_replace(
          "@([^a-zA-Z0-9\+\-\_\*\@\$\!\;\.\?\#\:\=\%\/\ ]+)@Ui",
          "",
          $content
        );
    }

    else return false;
  }
  
}
?>