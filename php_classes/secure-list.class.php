<?php
require 'secure.class.php';
class SecureList extends Secure {
  
  // properties
  private $ls;
  
  // methods
  public function SecureList() {
    $this->ls = array();
  }
  
  public function pushLs($name, $value=null) {
    if($value != null) {
      $value = $this->secureValue($value);
    } else $value = false;
    
    $this->ls[$name] = $value;
  }
  
  public function getLsFull() {
    return $this->ls;
  }
  public function getLs($lsIndex) {
    return $this->ls[$lsIndex];
  }
  
  public function delLs() {
    $this->ls = array();
  }
  
  
}
?>