/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var imgGridController = function ($scope, $http) {
	// --------------------------------------------
	// init
	// --------------------------------------------
	// once declaring all var will run init()
	function init() {
	// http.get .json
	// --------------------------------------------
	$http.get('../js/_json/'+webPage+'.json').success(function (data){
		$scope.data = data;
		 
			// start method 1
			console.log("fire start(jsonSuccess)");
			$scope.start("jsonSuccess");
	});
	// end .json
	
		// start method 2
		window.onpopstate = function () {
			console.log("fire start(onpopstate)");
			$scope.start("onpopstate");
		};
	}
	window.onload = function () {
		// Add close on background
		$('#fullScreen>.modal-backdrop').mousedown(function () {
			$scope.fullScreenOff();
		});
		
		// #fullWork*
		// the variables will be used multiple times
		// so for cleaner processing is placed here
		// see fullScreenOn()
			switch(webPage) {
				case "works":
					fullWorkNames = ["Title", "Type", "Material"];
					break;
				case "catalogues":
					fullWorkNames = ["Title", "Info"];
					break;
				default : // used for "installations" and "performances"
					fullWorkNames = ["Title", "Place", "Info"];
					break;
			}
			// creates associative array of getElementByIds #fullWork*
		for(var i=0; i<fullWorkNames.length; i++) {
			fullWork[fullWorkNames[i].toLowerCase()] =
				document.getElementById("fullData"+fullWorkNames[i]);
		}
	};
	// --------------------------------------------
	// end init
	// --------------------------------------------
	
	// global variables
	// --------------------------------------------
	// var used on other function
	// see fullScreenOn()
	// see init()
	var fullWorkNames, fullWork = {};

	// --------------------------------------------
	// fullScreen
	// --------------------------------------------
	
	$scope.fullNoOn = function (Number) {
		$scope.fullWork = $scope.data[Number];
	};
	$scope.fullScreenOn = function (myId) {
		// myId with trailing zeros
		myIdImg = $scope.toImgSrc(myId);
		// Change full screen infomation
		$scope.fullNo = parseInt(myId);
		
		// Add src to img
		document.getElementById('imgFullScreen').src = 
					'../images/'+webPage+'/full/'+myIdImg+'.jpg';
				
		// Insert information
		for(var i=0; i<fullWorkNames.length; i++) {
			var fullWorkNameLCase = fullWorkNames[i].toLowerCase();
			console.log("fullWorkNameLCase: "+ fullWorkNameLCase);
				console.log($scope.data[$scope.fullNo][fullWorkNameLCase]);
			fullWork[fullWorkNameLCase].innerHTML = 
				$scope.data[$scope.fullNo][fullWorkNameLCase];
		}
			// Size information used differently
if(webPage === "works") {
		document.getElementById("fullDataSize").innerHTML =
			$scope.sizeCm($scope.fullNo);
}
			// Ownership information also different
if(webPage === "works" || webPage === "catalogues") {
			function getOwner() {
				var owner = $scope.data[$scope.fullNo].owner;
				switch(owner) {
					case "false":
						return "for <span style='color:red;'>SALE</span>";
						break;
					case "true":
						return "<span style='text-decoration: underline;'>not for sale</span>";
						break;
					default:
						// for possible later use
						// to differentiate between buyer and price
						// if(owner === parseNumber(owner) ) {}
						if (owner === undefined) return "";
						return owner;
						break;
				};
			}
		document.getElementById("fullDataOwner").innerHTML = getOwner();
}
		
		document.getElementById("fullDataYear").innerHTML = 
				$scope.ifNotZero($scope.data[$scope.fullNo].year);
		
		// Show full screen
		$('#fullScreen').modal();
		
		// Change history and location in DOM
		if($scope.headUrl !== undefined) {
			var myUrl = window.location.href;
			
			// debugging: "Error: $scope.data[myId] is undefined
			// noticed $scope.data[] is not fully startialized at this state, thus the AngularJS error
			
			if($scope.data !== undefined) {
				urlAndTitleAJAX(
					// define headUrl
					$scope.headUrl+'#!'+myIdImg,
					// define <title> tag
					$scope.data[parseInt(myId)].title+' | '+$scope.title
				);
			} else {
				urlAndTitleAJAX(
					// define headUrl
					$scope.headUrl+'#!'+myIdImg,
					// define <title> tag
					$scope.title
				);
			}
		} else {
			// error logs
			if($scope.headUrl === undefined)
				console.log('$scope.headUrl is undefined');
			
			else //$scope.data === undefined
				//added as else to prevent promise to catchup
				console.log('$scope.data[] is undefined');
		}
	};
	
	// resetting url and title
	$scope.fullScreenOff = function () {
		urlAndTitleAJAX(
			$scope.headUrl,
			$scope.title
		);
	};
	
	// stops onpopstate enter
	$scope.start = function (howFired) {
	//console.log("start() has fired");
		var isFirstTime = $scope.headUrl === undefined;
		var myUrl = window.location.href;
		
		// should be '<www.my.site>/works/'
		$scope.headUrl = 
					myUrl.substring(0, myUrl.lastIndexOf("/")
					).toString() + '/';
		
		// make a root title
		if(isFirstTime) {
			$scope.title = document.title;
		}
		
		// if value in url
		if($scope.headUrl !== myUrl) {
			var givenId = myUrl.substring(
						myUrl.lastIndexOf("/#!")+3 );
			console.log("givenId"+ givenId);
			$scope.fullScreenOn(givenId);
		} else {
			$('#fullScreen').modal('hide');
			
		}
		
	// 
		
	}; // end start
	
	$scope.toImgSrc = function (num) {
		return leadingZeros(num, 3);
	};
	
	
	
	
	
	// --------------------------------------------
	// end fullScreen
	// --------------------------------------------
	
	// Other functions uncatagorized
	// --------------------------------------------
	// outputs values with cm 
	// where value are not zero
	$scope.sizeCm = function (id) {
		var size = $scope.data[id].size;
		var sizes = [size.X, size.Y, size.Z];
		var result = "";
		for(var i=0; i<sizes.length; i++) {
			if(sizes[i] === 0){
				result += "";
			} else {
				result += sizes[i] + "cm ";
			}
		}
		return result;
	};
	$scope.ifNotZero = function (int) {
		if (int === 0) {
			return;
		} else {
			return int;
		}
	}
	// end Other functions
	
	// Filtering
	// --------------------------------------------
	$scope.searchType = [false, false, false];
	$scope.filterType = function () {
		return function (data) {
			
			var result = false; // for the sake of for()s
			var myType = $scope.searchType; // more easy name
			
			var none = [false, false, false];
			// as directly comparing arrays is not possible
			if(myType.toString() === none.toString()) {
				return true;
			}
			
			for(var i=0; i<myType.length; i++) {
				if(
						data.type === myType[i] && 
						result === false
				) {
					result = true;
				}
			}
			return result;
		};
	};
	// end Filtering
	
	init();
}; // end $scope


function urlAndTitleAJAX(urlPath, pageTitle){
	document.title = pageTitle;
	window.history.pushState(
		{"pageTitle": pageTitle},
		pageTitle,
		urlPath
  );
}

function leadingZeros(num, toLength) {
	num += "";
	while(num.length < toLength) {
		num = "0" + num;
	}
	return num;
}
