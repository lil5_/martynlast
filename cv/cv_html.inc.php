<h2>Martyn Last	(b. 1961)</h2>
<h4>
Education
</h4>
<h4>
Rijksakademie van beeldende kunsten,
<br/>Amsterdam, NL, 1979-1982
</h4>
<h4>
Solo exhibitions
</h4>
<table class="table">
<tr><th>
2015
</th><td>
Opnar Sýningu Í Alþýðuhúsinu, Iceland
</td></tr>
<tr><th>
2014
</th><td>
Arttra, Amsterdam, NL. ‘The universe always wins’
</td></tr>
<tr><th>
2012
</th><td>
Boekie Woekie, Amsterdam, NL. ‘The Examinations’
</td></tr>
<tr><th>
2012
</th><td>
Arttra, Amsterdam, NL.´Sculptura Sculptura!´
</td></tr>
<tr><th>
2010
</th><td>
De Kring, Amsterdam, NL. ‘Neuszoenen & Videodozen’
</td></tr>
<tr><th>
2007
</th><td>
Suzanne Biederberg Gallery, Amsterdam, NL. ‘PRE-vue’
</td></tr>
<tr><th>
2007
</th><td>
Boekie Woekie Amsterdam, NL. ‘Martyn Last in Boekie Woekie’
</td></tr>
<tr><th>
2004
</th><td>
Galerie Parade Amsterdam, NL. ’De nacht van’
</td></tr>
<tr><th>
2004
</th><td>
Buro Empty, Amsterdam, NL.
</td></tr>
<tr><th>
1999
</th><td>
Suzanne Biederberg Gallery, Amsterdam, NL. ‘Personal Worlds, Private Works’
</td></tr>
<tr><th>
1997
</th><td>
Suzanne C. Walther Gallery, Stuttgart, DE.
</td></tr>
<tr><th>
1996
</th><td>
Richard Salmon Gallery, London, GB.
</td></tr>
<tr><th>
1993
</th><td>
Municipal Museum, Trivandrum, IN.
</td></tr>
<tr><th>
1989
</th><td>
Galerie Stelling, Leiden, NL.
</td></tr>
<tr><th>
1987
</th><td>
Galerie Holleeder, Amsterdam, NL.
</td></tr>
</table>
<h4>
Group exhibitions
</h4>
<table class="table">
<tr><th>
2014
</th><td>
De Kring, Amsterdam, NL ¨De Saloon¨
</td></tr>
<tr><th>
2014
</th><td>
Loods 6 / Go Gallery, Amsterdam, NL. ¨Mythe & muze¨
</td></tr>
<tr><th>
2013
</th><td>
Studio 73, London, UK.
</td></tr>
<tr><th>
2013
</th><td>
Bankstreet Arts, Sheffield, UK.
</td></tr>
<tr><th>
2012
</th><td>
DR Academy, Institute for X, Aarhus, DK.
</td></tr>
<tr><th>
2012
</th><td>
De Kring, Amsterdam, NL. ´Haal De Kring in Huis´
</td></tr>
<tr><th>
2011
</th><td>
England & Co, London, UK. ‘Old & New: A kind of Equivalence´
</td></tr>
<tr><th>
2011
</th><td>
Molenpad 11, Amsterdam, NL. ‘De Geest van het Huis
</td></tr>
<tr><th>
2011
</th><td>
De Kring, Amsterdam, NL. ‘Mijmeringen’
</td></tr>
<tr><th>
2011
</th><td>
De Veemvloer, Amsterdam, NL. “Bundelingen”
</td></tr>
<tr><th>
2010
</th><td>
KEG-Schijndel, NL. ‘Samenbundelingen.’
</td></tr>
<tr><th>
2010
</th><td>
Openstal, Oldeberkoop, NL. ‘Wat een portret.’
</td></tr>
<tr><th>
2010
</th><td>
De Kring, Amsterdam,	NL. ‘Summer Show.’
</td></tr>
<tr><th>
2010
</th><td>
Dieter Roth Academy. Hjalteri, Iceland.
</td></tr>
<tr><th>
2009
</th><td>
DR Academy. Stuttgart, DE.
</td></tr>
<tr><th>
2008
</th><td>
Galerie Parade, Amsterdam, NL. ‘doolhoven en Labyrinten’
</td></tr>
<tr><th>
2008
</th><td>
Boekie Woekie, Amsterdam, NL.
</td></tr>
<tr><th>
2008
</th><td>
England & Co, London, GB. ‘Summer Show’
</td></tr>
<tr><th>
2008
</th><td>
England & Co, London, GB. ‘Is there anybody out there?’
</td></tr>
<tr><th>
2008
</th><td>
The Garej, Cardiff, GB. ‘Brake Pads’
</td></tr>
<tr><th>
2008
</th><td>
Arttra, Amsterdam, NL. ‘Kosmisch Bewustzijn’
</td></tr>
<tr><th>
2008
</th><td>
De Vishal, Haarlem, NL, ‘Avatars’
</td></tr>
<tr><th>
2007
</th><td>
De Kring, Amsterdam, NL. ‘Lonkende Licht Fonteinen’
</td></tr>
<tr><th>
2007
</th><td>
Galerie Parade, Amsterdam, NL. ‘De Veertien Kruiswegstaties’
</td></tr>
<tr><th>
2006
</th><td>
FS29, Amsterdam, NL. ‘Werken op Papier en Uitzonderingen’
</td></tr>
<tr><th>
2006
</th><td>
FS29 Stichting AKKA, Amsterdam	, NL.’De Geur van Licht’
</td></tr>
<tr><th>
2004
</th><td>
Galerie Parade, Amsterdam, NL. ‘Wit Weiss White Blanc’
</td></tr>
<tr><th>
2004
</th><td>
Lighthouse, Centre for the Arts, Poole, GB. ’I AM NOT A TOY’
</td></tr>
<tr><th>
2003
</th><td>
The Winchester Gallery, Winchester, GB. ’Bad Quality’, and on tour to New Greenham Arts, New Bury, GB. The Millias Gallery, Southampton, GB.
</td></tr>
<tr><th>
2002
</th><td>
Buro Empty, Amsterdam, NL. ‘Basic-input-output-system, vacancy be tied down’
</td></tr>
<tr><th>
2002
</th><td>
BovenIJ Ziekenhuis, Amsterdam, NL. ‘Healing Art of Art’, curated by Buro Empty, Amsterdam
</td></tr>
<tr><th>
2001
</th><td>
England & Co, London, GB. ‘The Map is Not the Territory’
</td></tr>
<tr><th>
2001
</th><td>
Galerie Parade, Amsterdam, NL. ‘Illusie’
</td></tr>
<tr><th>
2001
</th><td>
Brighton Media Centre Gallery, Brighton, GB.
</td></tr>
<tr><th>
1999
</th><td>
Gasworks Gallery, London, GB. ‘Passion’
</td></tr>
<tr><th>
1999
</th><td>
Sotheby’s, London, GB. ‘Chocolate’, and on tour to Inverness Museum, Newcastle-under-Lyme Art
Gallery and Museum, Mansfield Art Gallery Nottinghamshire
</td></tr>
<tr><th>
1999
</th><td>
Stuff Gallery, London, GB.
</td></tr>
<tr><th>
1999
</th><td>
B16 Gallery, Birmingham, GB. ‘Xn’
</td></tr>
<tr><th>
1998
</th><td>
Collins Gallery, Glasgow, GB. ‘Chocolate’, and on tour to Summerlee Heritage Centre, Rochdale Museum, Aylesbury
</td></tr>
<tr><th>
1997
</th><td>
Richard Salmon Gallery, London, GB. ‘Sequence’
</td></tr>
<tr><th>
1997
</th><td>
The Wallsall Museum and Art Gallery, GB. ‘Plastic’
</td></tr>
<tr><th>
1996
</th><td>
Arnolfini Gallery, Bristol, GB. ‘Plastic’
</td></tr>
<tr><th>
1996
</th><td>
Richard Salmon Gallery, London, GB. ‘Plastic’
</td></tr>
<tr><th>
1995
</th><td>
Galerie de la Tour, Amsterdam, NL. ‘Summer show’
</td></tr>
<tr><th>
1994
</th><td>
The Wellcome Foundation, London, GB. ‘Eyes Abroad’, organized by the Courtauld Institute.
</td></tr>
<tr><th>
1994
</th><td>
Les Pyramides, Brussel, BE. ‘Art is a Trick’
</td></tr>
<tr><th>
1993
</th><td>
La Baratorio 66, Milano, I. ´Artist Books´
</td></tr>
<tr><th>
1992
</th><td>
Melkweg Galerie, Amsterdam, NL. ‘Aids Related Art’
</td></tr>
<tr><th>
1991
</th><td>
Gallery Crist Deleat, Antwerpen, BE.
</td></tr>
<tr><th>
1990
</th><td>
Shurini Gallery, London, GB.
</td></tr>
<tr><th>
1990
</th><td>
Peter Scott Gallery/Lancaster University, Lancaster, GB. ‘8x8x4’
</td></tr>
<tr><th>
1989
</th><td>
Galerie Stelling, Leiden, NL. ‘The Ultimate View’
</td></tr>
<tr><th>
1989
</th><td>
Blenheim/Tabernacle Gallery, London, GB.
</td></tr>
<tr><th>
1988
</th><td>
Bussum, NL. ‘Tuinen in Beeld’
</td></tr>
<tr><th>
1988
</th><td>
Galerie Holleeder, Amsterdam, NL.
</td></tr>
</table>
<h4>
Art fairs
</h4>
<table class="table">
<tr><th>
2013
</th><td>
Small Publishers Fair. London, GB.
</td></tr>
<tr><th>
2012
</th><td>
Le Saloon du Livre, Geneve, CH.
</td></tr>
<tr><th>
2011
</th><td>
Art Book Fair, Amsterdam, NL.
</td></tr>
<tr><th>
2008
</th><td>
Book Fair London, GB.
</td></tr>
<tr><th>
2007
</th><td>
Art Amsterdam, NL.
</td></tr>
<tr><th>
2007
</th><td>
Art Rotterdam, NL.
</td></tr>
<tr><th>
2004
</th><td>
KunstRAI Amsterdam, NL.
</td></tr>
<tr><th>
2002
</th><td>
Artissima 2002, Torino, IT.
</td></tr>
<tr><th>
2001
</th><td>
KunstRAI, Amsterdam, NL.
</td></tr>
<tr><th>
1998
</th><td>
The London Contemporary Art Fair, London, GB. ‘Art ‘98’.
</td></tr>
</table>
<h4>
Collaborations/Special events
</h4>
<table class="table">
<tr><th>
2014
</th><td>
De Kringprent No. 4. NL.
</td></tr>
<tr><th>
2014
</th><td>
Windpark Sohrewald, Kassel, DE. H. Wijnen ¨Between¨
</td></tr>
<tr><th>
2012
</th><td>
Aabke, Aarhus, DK. ‘The DRA Examination Box’
</td></tr>
<tr><th>
2012
</th><td>
De kunstenaar en zijn collectie: J. Bisschops Arti et Amicitia,  Amsterdam, NL.
</td></tr>
<tr><th>
2011
</th><td>
Veemtheater, Amsterdam, NL. H. Wijnen NL. “25 Jaar’
</td></tr>
<tr><th>
2011
</th><td>
De Kring; Amsterdam, NL. ‘Placemat’
</td></tr>
<tr><th>
2011
</th><td>
Boekie Woekie, H. Wijnen. Amsterdam, NL. ’Strak tegen Elkaar’
</td></tr>
<tr><th>
2010
</th><td>
Museum Frankenberg, H. Wijnen. Frankenberg, DE. ‘Turbulenzen’
</td></tr>
<tr><th>
2009
</th><td>
GeoPark, H. Wijnen Waldeck-Frankenberg, DE. ‘Bewegterwind’
</td></tr>
<tr><th>
2008
</th><td>
Amsterdammertjes Parade, Amsterdam, NL.
</td></tr>
<tr><th>
2008
</th><td>
Boekie Woekie & Co, ‘My Live as a Sandwich Man’
</td></tr>
<tr><th>
2008
</th><td>
12e Editie Glasrijk, H. Wijnen, Tubbergen, NL.
</td></tr>
<tr><th>
2007
</th><td>
11e Editie Glasrijk H. Wijnen, Tubbergen, NL.
</td></tr>
<tr><th>
2004
</th><td>
3 Hoog, ’Wunderkamer’, L. Couwenberg, Amsterdam, NL.
</td></tr>
<tr><th>
1997
</th><td>
Hollenbach Galerie, Stuttgart, DE.
</td></tr>
<tr><th>
1996
</th><td>
Musée d’art Moderne de la Ville de Paris, Paris, FR. ‘Life/Live show’,
collaboration; Fingers Mu Chi and the Wallpaper Table of all Places, collaboration with David Medalla
</td></tr>
<tr><th>
1991
</th><td>
Museum Het Kruithuis, Den Bosch, NL. ‘Double Distance’
</td></tr>
<tr><th>
1990
</th><td>
Performance Hayles St, London, GB.
</td></tr>
<tr><th>
1988
</th><td>
Uriot Prijs
</td></tr>
</table>
<h4>
Publications
</h4>
<table class="table">
<tr><th>
2012
</th><td>
Het André Behr Pamphlet 21
</td></tr>
<tr><th>
2011
</th><td>
De Bank Directeur
</td></tr>
<tr><th>
2011
</th><td>
Last & Wijnen. ‘ Strak tegen Elkaar’
</td></tr>
<tr><th>
2006
</th><td>
'The Last Book Box'
</td></tr>
<tr><th>
2005
</th><td>
'Een briev uit Switserland'
</td></tr>
<tr><th>
1997
</th><td>
‘Letter Drawings & Faxes’
</td></tr>
<tr><th>
1994
</th><td>
‘A collection of trinkets’
</td></tr>
</table>
<h4>
Articles
</h4>
<table class="table">
<tr><th>
2014
</th><td>
Alert, No. 9, “The universe  always wins” van Martyn Last.
</td></tr>
<tr><th>
2014
</th><td>
Kringblad, Terug in het scheve huis. L van Genugten
</td></tr>
<tr><th>
2011
</th><td>
Circulaire, No. 6 Placemat
</td></tr>
<tr><th>
2010
</th><td>
De Kring, ‘Neuszoenen & Videodozen.”
</td></tr>
<tr><th>
1997
</th><td>
The Art Newspaper, GB, ‘Glamour’s consequence’
</td></tr>
<tr><th>
1993
</th><td>
The Hindu, IN, ‘Discovering beauties of the East’, Bindu Bhaskar
</td></tr>
<tr><th>
1988
</th><td>
Krises, Bert Jansen, No. 6
</td></tr>
</table>
<h4>
Catalogues
</h4>
<table class="table">
<tr><th>
2014
</th><td>
7. Bewegter wind, ¨between¨. Kassel. DE.
</td></tr>
<tr><th>
2013
</th><td>
Banksteet Arts, UK.
</td></tr>
<tr><th>
2012
</th><td>
DRA, Meeting at the Institute for X, Aarhus, DK.
</td></tr>
<tr><th>
2012
</th><td>
Martyn Last ¨All the shit passes through the house of Pinocchio¨
</td></tr>
<tr><th>
2011
</th><td>
Last & Wijnen. ‘Strak tegen Elkaar’ De Trompetter Reeks.
</td></tr>
<tr><th>
2010
</th><td>
Waldeck-Frankenberg, Turbulenzen, DE.
</td></tr>
<tr><th>
2010
</th><td>
‘Samenbundelingen.’ Schijndel. NL.
</td></tr>
<tr><th>
2010
</th><td>
‘Wat een portret’ Oldeberkoop. NL.
</td></tr>
<tr><th>
2008
</th><td>
Geopark, ‘Bewegterwind’ DE.
</td></tr>
<tr><th>
2008
</th><td>
‘Glasrijk Tubbergen’ Glaskunst Tubbergen, NL.
</td></tr>
<tr><th>
2007
</th><td>
‘Glasrijk Tubbergen’ Glaskunst Tubbergen, NL.
</td></tr>
<tr><th>
2006
</th><td>
‘Martyn Last-- Concealingly Revealing’, Bert Jansen, Cralan Kelder, Zone 4 London, GB.
</td></tr>
<tr><th>
2001
</th><td>
‘The Map is Not the Territory’, England & Co, London, GB.
</td></tr>
<tr><th>
1998
</th><td>
‘Chocolate’, Francis Mckee, Collins Gallery/University of Strathclyde, Glasgow, GB.
</td></tr>
<tr><th>
1996
</th><td>
‘Martyn Last – Paintings and drawings’, Guy Brett, Eli Lei, Richard Salmon Gallery, London, GB.
</td></tr>
<tr><th>
1996
</th><td>
‘Plastic’, Edwardes Square Studios/Arnolfini Gallery, Bristol/London, GB.
</td></tr>
<tr><th>
1994
</th><td>
‘Eyes Abroad’, The Wellcome Foundation Ltd, London, GB.
</td></tr>
<tr><th>
1990
</th><td>
‘An Exhibition of Artist Boxes’, R. Williams-P.Hatton Lancaster University
</td></tr>
<tr><th>
1988
</th><td>
‘Tuinen in Beeld’, P. Deiters - H. Schoonhoven
</td></tr>
</table>
<h4>
Reviews
</h4>
<table class="table">
<tr><th>
2014
</th><td>
De Volkskrant, NL. ¨The universe always wins” 03-10
</td></tr>
<tr><th>
1999
</th><td>
The Sunday Telegraph, GB. “‘Sick’ exhibition of chocolate body parts causes an outcry”, Catherine Milner
</td></tr>
<tr><th>
1997
</th><td>
DerStuttgarter Zeitung, DE. ‘Leichenblaß in leerer Mitte’
</td></tr>
<tr><th>
1997
</th><td>
AN Magazine - 9 ‘Sequence’
</td></tr>
<tr><th>
1996
</th><td>
The Guardian, GB. ‘Plus the joy of Plastic’
</td></tr>
<tr><th>
1996
</th><td>
Time Out, GB. ‘Martyn Last, David Cheeseman’, Martin Coomer 11-18 December
</td></tr>
<tr><th>
1996
</th><td>
Art monthly, ’plastic’, R. Garnett
</td></tr>
<tr><th>
1993
</th><td>
The Mathrapunky
</td></tr>
<tr><th>
1989
</th><td>
De Uitkrant, O. Schilstra
</td></tr>
<tr><th>
1989
</th><td>
Het Leidse Dagblad, H. Stoop
</td></tr>
<tr><th>
1988
</th><td>
De Gooi en Eemlander, 28/5
</td></tr>
<tr><th>
1988
</th><td>
Het Financieel Dagblad, 7/5
</td></tr>
<tr><th>
1987
</th><td>
De Volkskrant, 7/3
</td></tr>
</table>
