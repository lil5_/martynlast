<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$slides = [
	[ 'name'=> 'First Image',			'url'=> '000' ],
	[ 'name'=> 'Second Image',		'url'=> '001' ],
	[ 'name'=> 'Third Image',			'url'=> '002' ]
	//{ 'name':	'url': }
];
?>

<div class="container"><div class="row">
	<div id="carousel-home" class="carousel slide fadding" data-ride="carousel" data-interval="4000">
		<!-- Indicators -->
		<!-- not sure if I'll keep Indicators
		<ol class="carousel-indicators hidden-sm hidden-xs hide">
			<li data-target="#carousel-home" data-slide="prev"></li>
			<li data-target="#carousel-home" data-slide="blank"></li>
			<li data-target="#carousel-home" data-slide="next"></li>
		</ol>-->
		
		<!-- Wrapper of slides -->
		<div class="carousel-inner" role="listbox">
<?php
for($i=0; $i<count($slides); $i++){
?>
			<div class="item <?php if($i==0) { ?>active<?php } ?>">
				<div class="col-md-10 col-md-offset-1">
					<img src="images/home/<?= $slides[$i]['url'] ?>.jpg" alt="martynLast_home_<?= $i + 1 ?>">
				</div>
			</div>
<?php } ?>
		</div>
		
		<!-- Controls -->
	<a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
		
	</div>
		
</div></div>
