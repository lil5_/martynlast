# MartynLast website #

The developer's repository.

### To-Do list ###

* Image on photoshop for tweaks
* Image resizing for icon and full
* Starting on Installations, Catalogues and Performances

### Pages ###
* Home
* Installations
* Works
* Catalogues
* Performances
* About
* CV

### Techniques ###
* HTML5
* CSS3
* JavaScript
* PHP
* Sassy CSS
* AngularJS
* Bootstrap 3
* jQuery