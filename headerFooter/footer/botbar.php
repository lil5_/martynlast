<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

	<nav id="botbar" class="navbar navbar-default headerFooter">
		<div class="container">
			<div class="row">

				<div class="col-sm-6">
					<div class="media">
						
						<div class="media-left media-middle">
							<img class="media-object" src="<?= $webRoot ?>images/_web/martyn_last_icon.jpg" alt="Martyn Last">
						</div>
						<div class="media-body">
							<h4 class="media-heading">Martyn Last</h4>
Amsterdam, the Netherlands<br/>
						</div>
						
					</div>
				</div>
				<div class="col-sm-6"><div class="row">
					<?php
$socialLinks = [
	'name' => ['Facebook', 'LinkedIn', 'GMail'], 
	'link' => [
		'https://www.facebook.com/profile.php?id=100009063791857', 
		'https://www.linkedin.com/profile/view?id=414085873', 
		'mailto:martynlast@gmail.com'
	]
];
for ($i=0; $i<count($socialLinks['name']); $i++) {
	?>
					<a class="col-xs-4 social aLikeDiv"
							 href="<?= $socialLinks['link'][$i] ?>">
						<img alt="<?= $socialLinks['name'][$i] ?>"
								 class="img-circle img-responsive img-ratio-responsive"
								 src="<?= $webRoot ?>images/_web/<?= strtolower(
										$socialLinks['name'][$i]
									) ?>.png">
						<h5 class="text-center"><?= $socialLinks['name'][$i] ?></h5>
					</a>
<?php } ?>
					</div></div>
				</div>

				<div class="row">
					<div class="col-sm-4 col-sm-push-8">
						<h6 class="text-right">
							<small>Special thanks to:</small><br/>
							<a href="http://glyphicons.com">GLYPHICONS.com</a><br/>
							<a href="http://dakirby309.deviantart.com/art/Metro-UI-Icon-Set-725-Icons-280724102">dAKirby309</a><br/>
							<a href="http://www.1001freefonts.com/another_typewriter.font">Johan Holmdahl</a>
						</h6>
					</div>
					<div class="col-sm-4">
						<h5 class="text-center"><br/>&copy; Lucian I. Last</h5>
					</div>
				</div>

			</div>
		</div>	
	</nav>