<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * To be placed inside the <body> tag.
 * 
 * $webRoot:String
 */
?>
		<!-- Include documents in end of <body> -->
<!-- Holder.js development -->
<!-- Bug solve -->
<img class="hide" data-src="holder.js/0x0">
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"> </script>-->
<script src="<?= $webRoot ?>js/_lib/holder.min.js"> </script>

<!-- jQuery core min -->
<!--<script src="https://code.jquery.com/jquery-2.1.3.min.js"> </script>-->
<script src="<?= $webRoot ?>js/_lib/jquery-2.1.3.min.js"> </script>

<!-- Bootstrap Javascript -->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"> </script>-->
<script src="<?= $webRoot ?>js/_lib/bootstrap.min.js"> </script>