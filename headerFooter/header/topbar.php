<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * $webRoot:String
 * $webTitleNo:Integer
 */

//$webPages = [
//	'Home',
//	'Installations',
//	'Works',
//	'Catalogues',
//	'Performances',
//	'About',
//	'CV'
//];
?>
<nav id="topbar" class="navbar navbar-default headerFooter">
	<div class="container">
		
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
			data-toggle="collapse" data-target="#topbarContent">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?= $webRoot ?>"
				 title="Home">
				Martyn Last
			</a>
		</div>
		
		<div class="collapse navbar-collapse" id="topbarContent">
			<ul class="nav navbar-nav navbar-right alt-font">
<?php 
$webPages = [
	[	'name'=>'Home',							'url'=>'home',					],
	[	'name'=>'Installations',		'url'=>'installations',	],
	[	'name'=>'Works',						'url'=>'works',					],
	[	'name'=>'Catalogues',				'url'=>'catalogues',		],
	[	'name'=>'Performances',			'url'=>'performances',	],
	[	'name'=>'About',						'url'=>'about',					],
	[	'name'=>'CV',								'url'=>'cv',						]
];


for($i=1; $i<count($webPages); $i++) {
?>
				<li class="<?php
	if($webTitleNo === $i) { // is same as webpage
		echo ' active';
	}
				?>">
					<a href="<?= $webRoot . $webPages[$i]['url'] . '/' ?>">
						<?= $webPages[$i]['name'] ?>
					</a>
				</li>
<?php } ?>
			</ul>
		</div>
		
	</div>
</nav>