<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * To be placed inside the <head> tag.
 * 
 * $webTitle:String
 * $webRoot:String
 */
?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"><!-- Stops browser from changing window size -->

<!-- Info search engine -->
<meta name="msvalidate.01" content="0EA12010E26993C727612EFAC758CD0F" />
<meta name="description" content="Martyn Last heeft in de loop der jaren een veelzijdig oeuvre opgebouwd. Zijn passie voor de beeldhouwkunst en zijn eigen levensloop zijn vaste uitgangspunten. Hij gebruikt verschillende materialen, zoals steen, brons, hout en papier.">
<meta name="author" content="Lucian I. Last">

<?php

if(!(
	!isset($angularJS) || 
	$angularJS == '' || 
	$angularJS == NULL
)) {
?>
<!-- AngularJS -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.min.js"> </script>-->
<script src="<?= $webRoot ?>js/_lib/angular.min.js"> </script>
	<!-- Controllers -->
	<?php // javascript inline webPage defined
	if($angularJS === 'imgGrid') {
		echo '<script>';
		echo 'var webPage = "'.strtolower($webTitle).'";';
		echo '</script>';
	} ?>
	<script src="<?= $webRoot ?>js/_angularjs/<?= $angularJS ?>_controller.js"> </script>
<?php } ?>
	
<!-- CSS -->
<link rel="stylesheet" href="<?= $webRoot ?>css/myGlobal.css">

<?php // page unique css link
$css = null;
if($angularJS == 'imgGrid') {
	$css = $angularJS;
} else {
	switch ($webTitle) {
		case 'Home':
		case 'CV':
			$css = strtolower($webTitle);
			break;
	}
}

if ($css !== null) { ?>
	<link rel="stylesheet" href="<?= $webRoot ?>css/<?= $css ?>.css">
<?php } ?>



<?php $webFontRoot = $webRoot; ?>
<style>
@font-face {
	font-family: "Glyphicons Halflings";
	src: url("<?= $webFontRoot ?>fonts/bootstrap/glyphicons-halflings-regular.eot?#iefix") format("embedded-opentype"), url("<?= $webFontRoot ?>fonts/bootstrap/glyphicons-halflings-regular.woff2") format("woff2"), url("<?= $webFontRoot ?>fonts/bootstrap/glyphicons-halflings-regular.woff") format("woff"), url("<?= $webFontRoot ?>fonts/bootstrap/glyphicons-halflings-regular.ttf") format("truetype"), url("<?= $webFontRoot ?>fonts/bootstrap/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg");
}
@font-face {
	font-family: "TypeWriter";
	src: url("<?= $webFontRoot ?>fonts/atwriter.ttf") format("truetype");
}
/* http://www.1001freefonts.com/another_typewriter.font */
</style>

<!-- Browser -->
<!--<link rel="icon" href="<?= $webRoot ?>images/favicon.gif">-->		
<title><?php if($webTitle !== 'Home') {?>
<?= $webTitle ?> | <?php } ?> Martyn Last</title>